/*
 * Tile Routing Subsystem
 *
 * Access tiles from Base tile with dynamic address generation.
 *
 * Provides hooks for different telegram types:
 * - broadcast
 * - state
 * - screen
 * - firmware
 *
 * user provides processor functions for data received.
 *
 */

#include "tile_routing.h"
#include "main.h"
#include "FreeRTOS.h"
#include <string.h>

/*
 * own address in routing schema
 */
static uint32_t ownAddr;
/*
 * Unique address of the device
 */
static uint32_t ownDevAddr;

static void(*tr_stateProcessor)(uint8_t *data, uint8_t len);
static void(*tr_screenProcessor)(uint8_t *data, uint8_t len);
static void(*tr_firmwareProcessor)(uint8_t *data, uint8_t len);

static tr_return tr_processStateTelegram(tr_TelegramTypeDef *msg){

	assert_param(tr_stateProcessor);

	// check CRC
	if(true) {
		tr_stateProcessor(msg->payload, msg->len);
	} else {
		return TR_FAIL_CRC;
	}
	return TR_SUCCESS;
}

static tr_return tr_processScreenTelegram(tr_TelegramTypeDef *msg){

	assert_param(tr_screenProcessor);

	// check CRC
	if(true) {
		tr_screenProcessor(msg->payload, msg->len);
	} else {
		return TR_FAIL_CRC;
	}
	return TR_SUCCESS;
}

static tr_return tr_updateFirmwareTelegram(tr_TelegramTypeDef *msg){

	assert_param(tr_firmwareProcessor);

	// check CRC
	if(true) {
		tr_firmwareProcessor(msg->payload, msg->len);
	} else {
		return TR_FAIL_CRC;
	}
	return TR_SUCCESS;
}

/*
 * Return last port
 */
inline uint8_t tr_extractAddressLastRoute(uint32_t aAddress){
	uint8_t addr;
	// iterate through the address to get to the portion of
	for (uint8_t i=0; i<10; i++){
		addr = (aAddress << ( 30 - ( i * 3 ) ) );
		if( ( addr & 0x7 ) != 0)
			return addr;
	}
}

/*
 * Check if we are recipient of the message
 */
inline static uint8_t tr_ourAddress(uint32_t aAdress){
	return (ownAddr == aAdress);
}

/*
 * reverse triplets of address to get routing to master
 *
 * 1.2.4.5 -> 5.4.2.1
 */
uint32_t tr_reverseAddr(uint32_t aAddress){
	uint32_t newAddr;
	uint8_t port;
	for(uint8_t i=0; i<10; i++){
		port = (aAddress >> (30 - (i*3)) & 0x7);
		if(port != 0) {
			newAddr +=  port  >> (i*3);
		}
	}
	return newAddr;
}

void tr_setOwnAddr(uint32_t aAddr) {
	ownAddr = aAddr;
}

uint32_t tr_getOwnAddr() {
	return ownAddr;
}

/*
 * Process received messages
 *
 * Receives a message and process its content
 * Route to defined function
 */
void tr_processTelegram(tr_TelegramTypeDef *msg){

	assert_param(msg);

	switch(msg->type){
		case BROADCAST:
				tr_sendBroadcast(msg);
			break;
		case INITIALIZE:
				ownAddr = msg->destAddr;
				msg->sourceAddr = ownAddr;
				msg->destAddr = tr_reverseAddr(ownAddr);
				msg->len = sizeof(ownDevAddr);
				memcpy(msg->payload, &ownDevAddr, msg->len);
				tr_sendTelegram(msg);
			break;
		case STATE:
				tr_processStateTelegram(msg);
			break;
		case SCREEN:
				tr_processScreenTelegram(msg);
			break;
		case FIRMWARE:
				tr_updateFirmwareTelegram(msg);
			break;
		default:
			assert_param(false);
			break;
	}
}

void tr_sendBroadcast(tr_TelegramTypeDef *msg){

	// only transport broadcast messages
	assert_param(msg->type == BROADCAST);

	tr_sendTelegram(msg);
}

void tr_sendTelegram(tr_TelegramTypeDef *msg){

	assert_param(msg);

	assert_param(msg->destAddr);
}

