/*
 * Tile Routing Subsystem
 *
 * Access tiles from Base tile with dynamic address generation
 *
 * The routing address is described as a list of up to 10 hops
 * made from the port of the device on which the message went through
 *
 */

#ifndef __TILE_ROUTING_H__
#define __TILE_ROUTING_H__
#include <stdint.h>
// set to payload data width or -1 for dynamic payload allocation
#define TR_MAX_PAYLOAD 32
#define TR_NO_ROUTE 0

typedef enum {
	BROADCAST,
	INITIALIZE,
	STATE,
	SCREEN,
	FIRMWARE
} tr_TelegramType;

typedef enum {
	TR_ERROR,
	TR_SUCCESS,
	TR_FAIL_CRC,
} tr_return;

typedef struct {
	uint32_t destAddr;
	uint32_t sourceAddr;
	tr_TelegramType type;
#if TR_MAX_PAYLOAD == -1
	uint8_t* payload;
#else
	uint8_t payload[TR_MAX_PAYLOAD];
#endif
	uint8_t len;
	uint8_t crc8;
} tr_TelegramTypeDef;

#if TR_MAX_PAYLOAD == -1
#define TR_TELEGRAM_SIZE -1
#else
  #define TR_TELEGRAM_SIZE sizeof(tr_TelegramTypeDef)
#endif

void tr_setOwnAddr(uint32_t aAddr);
uint32_t tr_getOwnAddr();
void tr_processTelegram(tr_TelegramTypeDef *msg);
void tr_sendBroadcast(tr_TelegramTypeDef *msg);
void tr_sendTelegram(tr_TelegramTypeDef *msg);

#endif /* __TILE_ROUTING_H__ */
